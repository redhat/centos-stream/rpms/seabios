From 39c280452e101edb814989300e2815cee3ea75c4 Mon Sep 17 00:00:00 2001
From: Gerd Hoffmann <kraxel@redhat.com>
Date: Wed, 3 May 2023 10:36:32 +0200
Subject: [PATCH] display error message for blocksizes != 512

RH-Author: Gerd Hoffmann <None>
RH-MergeRequest: 6: log error message to screen when booting with (unsupported) 4k sectors
RH-Jira: RHEL-7110
RH-Acked-by: Miroslav Rezanina <mrezanin@redhat.com>
RH-Commit: [2/2] f4cdc59a968ed2cb795d0b7c357522423a16f4a2 (kraxel.rh/centos-src-seabios)

This actually happens in case users try to use 4k sectors with seabios.
Printing the error to the screen instead of only the debug log helps
users to figure why their guest doesn't boot.

Signed-off-by: Gerd Hoffmann <kraxel@redhat.com>

Patch-name: seabios-display-error-message-for-blocksizes-512.patch
Patch-id: 2
Patch-present-in-specfile: True
---
 src/hw/blockcmd.c   | 2 +-
 src/hw/virtio-blk.c | 4 ++--
 2 files changed, 3 insertions(+), 3 deletions(-)

diff --git a/src/hw/blockcmd.c b/src/hw/blockcmd.c
index 6b6fea97..ff88680b 100644
--- a/src/hw/blockcmd.c
+++ b/src/hw/blockcmd.c
@@ -336,7 +336,7 @@ scsi_drive_setup(struct drive_s *drive, const char *s, int prio)
     // 64-bit LBA anyway.
     drive->blksize = be32_to_cpu(capdata.blksize);
     if (drive->blksize != DISK_SECTOR_SIZE) {
-        dprintf(1, "%s: unsupported block size %d\n", s, drive->blksize);
+        hwerr_printf("%s: unsupported block size %d\n", s, drive->blksize);
         return -1;
     }
     drive->sectors = (u64)be32_to_cpu(capdata.sectors) + 1;
diff --git a/src/hw/virtio-blk.c b/src/hw/virtio-blk.c
index 137a2c3c..1d6f7d4b 100644
--- a/src/hw/virtio-blk.c
+++ b/src/hw/virtio-blk.c
@@ -193,8 +193,8 @@ init_virtio_blk(void *data)
             vdrive->drive.blksize = DISK_SECTOR_SIZE;
         }
         if (vdrive->drive.blksize != DISK_SECTOR_SIZE) {
-            dprintf(1, "virtio-blk %pP block size %d is unsupported\n",
-                    pci, vdrive->drive.blksize);
+            hwerr_printf("virtio-blk %pP block size %d is unsupported\n",
+                         pci, vdrive->drive.blksize);
             goto fail;
         }
         dprintf(3, "virtio-blk %pP blksize=%d sectors=%u size_max=%u "
